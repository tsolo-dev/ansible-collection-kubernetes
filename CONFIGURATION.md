# Roles


## tsolo.kubernetes/helm/argo_workflow

Install Argo Workflow

### artifacts_dir


#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### helm_argo_workflow_chart_version

The version of the Argo Workflow chart that will be installed.

#### Default value:

```yaml
helm_argo_workflow_chart_version: 5.0.0
```

### helm_argo_workflow_database

PostgreSQL database credentials used by Argo Workflow.

#### Default value:

```yaml
helm_argo_workflow_database:
  address: postgresql.srv
  database: argo
  password: argo
  username: argo
```

### helm_argo_workflow_hostname

Hostname used for Argo Workflow UI.

#### Default value:

```yaml
helm_argo_workflow_hostname: argo.{{ internal_domain }}
```

### helm_argo_workflow_sso_token

Keycloak SSO token for Argo client.

#### Default value:

```yaml
helm_argo_workflow_sso_token: 1235678901234567890abcdefabcdef
```
This is a secret, the value of production and development is not shown.

## tsolo.kubernetes/helm/ceph_csi_rbd



### helm_ceph_csi_rbd_chart_version


#### Default value:

```yaml
helm_ceph_csi_rbd_chart_version: 3.4.0
```

### helm_ceph_csi_rbd_key


#### Default value:

```yaml
helm_ceph_csi_rbd_key: xyz
```

### helm_ceph_csi_rbd_mons


#### Default value:

```yaml
helm_ceph_csi_rbd_mons:
- 10.0.0.1:6789
- 10.0.0.2:6789
- 10.0.0.3:6789
```

### helm_ceph_csi_rbd_pool


#### Default value:

```yaml
helm_ceph_csi_rbd_pool: rbd
```

### helm_ceph_csi_rbd_user


#### Default value:

```yaml
helm_ceph_csi_rbd_user: admin
```

## tsolo.kubernetes/helm/cert_manager

Install and Configure Cert-Manager

### helm_cert_manager_chart_version

The version of the Cert-Manager chart that will be installed.

#### Default value:

```yaml
helm_cert_manager_chart_version: v1.8.2
```

## tsolo.kubernetes/helm/csi_nfs



### helm_csi_nfs_chart_version

The version of the Helm Chart that will be installed.

#### Default value:

```yaml
helm_csi_nfs_chart_version: v4.1.0
```

## tsolo.kubernetes/helm/dask

Install Dask

### helm_dask_chart_version

Version of Dask Chart to install.

#### Default value:

```yaml
helm_dask_chart_version: 2022.6.1
```

## tsolo.kubernetes/helm/gitlab

Install GitLab on Kubernetes. This role is still work in progress (WIP).

### helm_gitlab_chart_version

The version of the GitLab chart that will be installed.

#### Default value:

```yaml
helm_gitlab_chart_version: 4.2.6
```

### helm_gitlab_hostname

The hostname of the GitLab service.

#### Default value:

```yaml
helm_gitlab_hostname: gitlab.{{ internal_domain }}
```

### helm_gitlab_storage_class

The storage class to use.

#### Default value:

```yaml
helm_gitlab_storage_class: nfs-csi
```

## tsolo.kubernetes/helm/grafana

Install Grafana the metrics visiulisation platform.

### grafana_keycloak_client_token

Shared secret between Keycloak and Grafana.

#### Default value:

```yaml
grafana_keycloak_client_token: 5c9ad8e43c65255a0feecfacf420bac57d6c300f71b9e850ebe423b31881dda5
```
This is a secret, the value of production and development is not shown.

### helm_grafana_admin_password

The default admin password.

#### Default value:

```yaml
helm_grafana_admin_password: admin
```
This is a secret, the value of production and development is not shown.

### helm_grafana_chart_version

The version of Grafana chart to use.
https://github.com/grafana/helm-charts/tags

#### Default value:

```yaml
helm_grafana_chart_version: 6.29.6
```

### helm_grafana_hostname

Hostname to use for Grafana.

#### Default value:

```yaml
helm_grafana_hostname: grafana.{{ internal_domain }}
```

### helm_grafana_tls_secret

Name of the secret object with TLS certificate and key.
If not defined or empty then Grafana will be deployed with HTTP not HTTPS.

#### Default value:

```yaml
helm_grafana_tls_secret: ''
```

### helm_keycloak_hostname

The hostname of Keycloak. See helm/keycloak role.
If not defined or empty then no external authenticator is used.

#### Default value:

```yaml
helm_keycloak_hostname: ''
```

## tsolo.kubernetes/helm/install



### helm_version

The version of Helm to use. Only versions prepackaged version from
https://quay.io/repository/tsolo/helm?tab=tags can be selected.

#### Default value:

```yaml
helm_version: v3.9.0
```

## tsolo.kubernetes/helm/jenkins

Install Jenkins CI

### helm_jenkins_chart_version

The version of the Jenkins chart that will be installed.

#### Default value:

```yaml
helm_jenkins_chart_version: 4.2.8
```

### helm_jenkins_hostname

The hostname of the jenkins service.

#### Default value:

```yaml
helm_jenkins_hostname: jenkins.{{ internal_domain }}
```

### helm_jenkins_storage_class

The storage class to use.

#### Default value:

```yaml
helm_jenkins_storage_class: nfs-csi
```

## tsolo.kubernetes/helm/jupyterhub

Install and configure JupyterHub

### helm_jupyterhub_chart_version

Version of the JupyterHub Helm chart to use.

#### Default value:

```yaml
helm_jupyterhub_chart_version: 1.2.0
```

### helm_jupyterhub_dask_gateway_api_token


#### Default value:

```yaml
helm_jupyterhub_dask_gateway_api_token: ''
```
This is a secret, the value of production and development is not shown.

### helm_jupyterhub_extra_config

Additional configuration to pass to JupyterHub.
This value is assigned to *hub.extraConfig* in the JupyterHub Chart.
The contents is converted to YAML when applied, thus .

#### Default value:

```yaml
helm_jupyterhub_extra_config: {}
```

### helm_jupyterhub_hostname


#### Default value:

```yaml
helm_jupyterhub_hostname: jupyterhub.{{ internal_domain }}
```

### helm_jupyterhub_hub_postgresql

Database credentials. The database used by the Hub.

#### Default value:

```yaml
helm_jupyterhub_hub_postgresql:
  database: jupyterhub
  hostname: postgresql-ha-pgpool.database.svc.cluster.local
  password: jupyterhub
  port: 5432
  username: jupyterhub
```

### helm_jupyterhub_profileList


#### Default value:

```yaml
helm_jupyterhub_profileList:
- default: true
  description: Python, R, and Julia.
  display_name: Datascience environment
  kubespawner_override:
    image: jupyter/datascience-notebook:python-3.9.7
```

### helm_jupyterhub_proxy_address

The IP address to assign to the JupyterHub loadbalancer proxy.

#### Default value:

```yaml
helm_jupyterhub_proxy_address: ''
```

### helm_jupyterhub_shutdown_idle_hours

Single user instances are shutdown after they have been idle for sometime.
The cull process will stop images if they are inactive for a length of time.
This value is in hours and is converted to second by the deployment scripts.
See https://github.com/jupyterhub/jupyterhub-idle-culler#how-it-works for more details
regarding how the cull process works.

#### Default value:

```yaml
helm_jupyterhub_shutdown_idle_hours: 3
```

### helm_jupyterhub_singleuser_storage_class

The storage Class to use for single user instances

#### Default value:

```yaml
helm_jupyterhub_singleuser_storage_class: nfs-csi
```

### helm_jupyterhub_tls_secret

The name of the Kubernetes secret in the same namespace that will be used to setup HTTPS.
The secret should contain tls.crt and tls.key fileds for the certificate and keys.
If not set HTTP is configured.

#### Default value:

```yaml
helm_jupyterhub_tls_secret: ''
```

## tsolo.kubernetes/helm/keycloak



### helm_keycloak_address

The address assosiated with the running instance of Keycloak.

#### Default value:

```yaml
helm_keycloak_address: ''
```

### helm_keycloak_admin_password

The admin password for Keycloak.

#### Default value:

```yaml
helm_keycloak_admin_password: password
```
This is a secret, the value of production and development is not shown.

### helm_keycloak_chart_version

The version of the Keycloak chart to install.

#### Default value:

```yaml
helm_keycloak_chart_version: 9.2.11
```

### helm_keycloak_hostname

The hostname that will be used for Keycloak.

#### Default value:

```yaml
helm_keycloak_hostname: keycloak.{{ internal_domain }}
```

### helm_keycloak_ingress_class

Name of the ingress class to use.
Not used if helm_keycloak_address is set.

#### Default value:

```yaml
helm_keycloak_ingress_class: traefik
```

### helm_keycloak_management_password

The management password for Keycloak.

#### Default value:

```yaml
helm_keycloak_management_password: password
```
This is a secret, the value of production and development is not shown.

### helm_keycloak_namespace

Namespace to install Keycloak into.

#### Default value:

```yaml
helm_keycloak_namespace: identitymanagement
```

### helm_keycloak_postgresql

Database credentials for Keycloak.

#### Default value:

```yaml
helm_keycloak_postgresql:
  database: keycloak
  hostname: database
  password: keycloak
  port: 5432
  username: keycloak
```

### helm_keycloak_tls_secret

Secret with TLS key and certificate
Not used if helm_keycloak_address is set.

#### Default value:

```yaml
helm_keycloak_tls_secret: ''
```

## tsolo.kubernetes/helm/kubevious

Install Kubevious Kubernetes dashboard

### helm_kubevious_chart_version

The version of the Kubevious chart that will be installed.

#### Default value:

```yaml
helm_kubevious_chart_version: 1.0.10
```

### helm_kubevious_hostname


#### Default value:

```yaml
helm_kubevious_hostname: kubevious.{{ internal_domain }}
```

## tsolo.kubernetes/helm/loki

Install Loki the Log aggregation service from Grafana.

### helm_loki_chart_version

The version of the Loki chart to use.

#### Default value:

```yaml
helm_loki_chart_version: 2.13.2
```

### helm_loki_object_storage

The S3 storage to use for Loki.
The format for this connection string is

### helm_loki_retention_hours

The period in hours that logs should be kept. Loks older than this period will
be deleted at the next clean up cycle.

#### Default value:

```yaml
helm_loki_retention_hours: 72
```

### helm_loki_storage


#### Default value:

```yaml
helm_loki_storage: ''
```

### helm_promtail_chart_version

The version of the Promtail chart to use.

#### Default value:

```yaml
helm_promtail_chart_version: 6.2.1
```

## tsolo.kubernetes/helm/metallb



### helm_metallb_address_range


#### Default value:

```yaml
helm_metallb_address_range: 10.0.0.100-10.0.0.200
```

### helm_metallb_chart_version

The version of the MetalLB chart that will be installed.

#### Default value:

```yaml
helm_metallb_chart_version: 0.12.1
```

## tsolo.kubernetes/helm/mimir

Install Mimir the metrics storage service from Grafana.

### helm_mimir_chart_version

The version of the Mimir chart to use.

#### Default value:

```yaml
helm_mimir_chart_version: 3.0.0
```

### helm_mimir_retention_days

The period in days that metrics should be kept. Metrics older than this period will
be deleted at the next clean up cycle.

#### Default value:

```yaml
helm_mimir_retention_days: 370
```

### helm_mimir_s3_access_key


#### Default value:

```yaml
helm_mimir_s3_access_key: mimir
```

### helm_mimir_s3_alerts_bucket


#### Default value:

```yaml
helm_mimir_s3_alerts_bucket: mimir-alerts
```

### helm_mimir_s3_blocks_bucket


#### Default value:

```yaml
helm_mimir_s3_blocks_bucket: mimir-blocks
```

### helm_mimir_s3_host


#### Default value:

```yaml
helm_mimir_s3_host: 192.168.1.1:9000
```

### helm_mimir_s3_https_enabled


#### Default value:

```yaml
helm_mimir_s3_https_enabled: false
```

### helm_mimir_s3_ruler_bucket


### helm_mimir_s3_rules_bucket


#### Default value:

```yaml
helm_mimir_s3_rules_bucket: mimir-rules
```

### helm_mimir_s3_secret_key


#### Default value:

```yaml
helm_mimir_s3_secret_key: Ecvjdhfiuehfn3q84ddwekdno23
```

## tsolo.kubernetes/helm/nginx

Install Nginx controller for ingest.

### helm_nginx_chart_version

Nginx controller chart version to install.

#### Default value:

```yaml
helm_nginx_chart_version: 9.2.11
```

### helm_nginx_configmap

Configure the Ingest controller. According to
https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap
Changes to config map requires a restart to the Nginx controller.

#### Default value:

```yaml
helm_nginx_configmap:
  proxy-body-size: 100m
  proxy-buffer-size: 256k
  proxy-buffering: on
  proxy-buffers-number: '4'
  proxy-max-temp-file-size: 1024m
```

### helm_nginx_load_balancer_ip

The IP address that Treafik will listen on.
If not defined Loadbalaner will automaticaly asign address.

#### Default value:

```yaml
helm_nginx_load_balancer_ip: 192.168.1.1
```

### helm_nginx_oauth_client_token


#### Default value:

```yaml
helm_nginx_oauth_client_token: ssssss
```
This is a secret, the value of production and development is not shown.

### helm_nginx_vouch_domain

The domain Vouch will provide SSO for.

#### Default value:

```yaml
helm_nginx_vouch_domain: '{{ internal_domain }}'
```

### helm_nginx_vouch_enabled

If vouch should be installed with Nginx.
Not implemented Yet. Vouch is always installed at the moment.

#### Default value:

```yaml
helm_nginx_vouch_enabled: false
```

### helm_nginx_vouch_hostname

The hostname of the vouch service.

#### Default value:

```yaml
helm_nginx_vouch_hostname: vouch.{{ internal_domain }}
```

### helm_nginx_vouch_jwt_token

A token string used in JWT generation

#### Default value:

```yaml
helm_nginx_vouch_jwt_token: a238483487234623847264826348276348263482736482736482736428f
```
This is a secret, the value of production and development is not shown.

### helm_nginx_vouch_oauth_client_token

The OAuth client_secret used by Vouch.

#### Default value:

```yaml
helm_nginx_vouch_oauth_client_token: ''
```
This is a secret, the value of production and development is not shown.

### helm_nginx_vouch_tls_secret

The name of the Kubernetes Secret with the TLS certificates and key.

#### Default value:

```yaml
helm_nginx_vouch_tls_secret: ''
```

## tsolo.kubernetes/helm/openebs

Install OpenEBS storage server using Helm.

### helm_openebs_chart_version

The version of OpenEBS chart that will be installed.

#### Default value:

```yaml
helm_openebs_chart_version: 3.1.0
```

## tsolo.kubernetes/helm/postgresql_ha



### helm_postgresql_address


#### Default value:

```yaml
helm_postgresql_address: ''
```

### helm_postgresql_admin_password


#### Default value:

```yaml
helm_postgresql_admin_password: eawzVE0fX5
```
This is a secret, the value of production and development is not shown.

### helm_postgresql_ha_chart_version


#### Default value:

```yaml
helm_postgresql_ha_chart_version: 8.2.7
```

### helm_postgresql_password


#### Default value:

```yaml
helm_postgresql_password: secret
```
This is a secret, the value of production and development is not shown.

### helm_postgresql_repmgrPassword


#### Default value:

```yaml
helm_postgresql_repmgrPassword: cOaFLtaSt1
```

### helm_postgresql_storage_class


#### Default value:

```yaml
helm_postgresql_storage_class: local-hostpath
```

## tsolo.kubernetes/helm/postgresql_single



### helm_postgresql_single_chart_version


#### Default value:

```yaml
helm_postgresql_single_chart_version: 10.13.14
```

## tsolo.kubernetes/helm/prometheus

Install and configure Prometheus.

### helm_prometheus_alertmanager_hostname

Hostname to use for Prometheus Alertmanager service.

#### Default value:

```yaml
helm_prometheus_alertmanager_hostname: alertmanager.{{ internal_domain }}
```

### helm_prometheus_chart_version

Version of Prometheus chart to be installed using Helm.

#### Default value:

```yaml
helm_prometheus_chart_version: 35.0.2
```

### helm_prometheus_hostname

Hostname to use for prometheus service.

#### Default value:

```yaml
helm_prometheus_hostname: prometheus.{{ internal_domain }}
```

### helm_prometheus_retention

How long to retain metrics. When to remove old data.
Number of days.

#### Default value:

```yaml
helm_prometheus_retention: 32
```

### helm_prometheus_retention_size

Maximum size of metrics. The maximum number of bytes of storage blocks to retain.
The oldest data will be removed first. Defaults to 0 or disabled.
Units supported: B, KB, MB, GB, TB, PB, EB. Ex: "512MB".
Based on powers-of-2, so 1KB is 1024B.
Only the persistent blocks are deleted to honor this retention although WAL
and m-mapped chunks are counted in the total size.
So the minimum requirement for the disk is the peak space taken by the
wal (the WAL and Checkpoint) and chunks_head (m-mapped Head chunks)
directory combined (peaks every 2 hours).

#### Default value:

```yaml
helm_prometheus_retention_size: 1TB
```

## tsolo.kubernetes/helm/reflector



### helm_reflector_chart_version


#### Default value:

```yaml
helm_reflector_chart_version: 6.1.47
```

## tsolo.kubernetes/helm/traefik

Install Traefik proxy service.

### helm_traefik_chart_version

Traefik Chart version to install.

#### Default value:

```yaml
helm_traefik_chart_version: 10.21.1
```

### helm_traefik_dashboard_hostname

Hostname of Traefik dashboard.

#### Default value:

```yaml
helm_traefik_dashboard_hostname: traefik.{{ internal_domain }}
```

### helm_traefik_dashboard_password

Password to traefik dashboard.

#### Default value:

```yaml
helm_traefik_dashboard_password: password
```
This is a secret, the value of production and development is not shown.

### helm_traefik_load_balancer_ip

The IP address that Treafik will listen on.
If not defined Loadbalaner will automaticaly asign address.

#### Default value:

```yaml
helm_traefik_load_balancer_ip: 192.168.1.1
```

## tsolo.kubernetes/helm/values_merge



### mergedir


#### Default value:

```yaml
mergedir: /tmp
```

## tsolo.kubernetes/k0s/controller

Install K0s controller

### artifacts_dir

Where the kubernetes config files are placed. This is a path on the host
that Ansible run on.

#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

## tsolo.kubernetes/k0s/ha

Setup high availability (HA) access to the controllers. Participating
worker nodes proxy controller access and share a floating IP.


### k0s_ha_auth_password

Shared password used to authenticate HA Keepalive nodes to one another.

#### Default value:

```yaml
k0s_ha_auth_password: ryr3S2s0s
```
This is a secret, the value of production and development is not shown.

### k0s_ha_cidr

The IP address and netmask in CIDR format to use as the floating address
for high availability (HA). This address will be assigned to one of the
worker nodes.

#### Example

```yaml
k0s_ha_cidr: 192.168.11.30/24
```

#### Default value:

```yaml
k0s_ha_cidr: ''
```

### k0s_ha_participation_enabled

If a node should participate in the HA cluster.
This variable is only set at group and host vars level.
It will result in the host not being used for HA purposes.
If there are less than 3 eligible nodes the HA deployment will fail.

#### Default value:

```yaml
k0s_ha_participation_enabled: true
```

## tsolo.kubernetes/k0s/install

Install Kubernetes using K0s. K0s is a light pre-package and tested Kubernetes release.
If */data* exists and is a mounted filesystem then */data/k0s* will be used as the working directory for Kubernetes. The default path (*/var/lib/k0s*) will be an symbolic link to */data/k0s*. Similarly if */data* exists and is a mounted filesystem then */data/openebs* will be used as the working directory for OpenEBS. The default path (*/var/openebs*) will be an symbolic link to */data/openebs*. K0s places the kubelet working directory at */var/lib/k0s/kubelet*, there are some libraries that do not allow the kubelet directory to be set. A symbolic link from */var/lib/k0s/kubelet* to */var/lib/kubelet* is created.

### k0s_version

The version of K0s to install.

#### Default value:

```yaml
k0s_version: v1.22.9+k0s.0
```

## tsolo.kubernetes/k0s/postinstall

Tasks to perform after Kubernetes has been installed.

### k0s_node_labels

k0s_node_labels is a dictionary, the key value pairs will be applied as
labels in Kubernetes. Each node can have a k0s_node_labels defined or
normal Ansible variable definitions per group can be done.

#### Default value:

```yaml
k0s_node_labels: {}
```

## tsolo.kubernetes/k0s/remove



### destroy_k0s


#### Default value:

```yaml
destroy_k0s: false
```

## tsolo.kubernetes/kubectl/dashy



### kubectl_dashy_docker_image

The docker image to use for Dashy.

#### Default value:

```yaml
kubectl_dashy_docker_image: lissy93/dashy:latest
```

### kubectl_dashy_hostname

Hostname to use for Dashy.

#### Default value:

```yaml
kubectl_dashy_hostname: dashy.{{ internal_domain }}
```

### kubectl_dashy_namespace

Namespace to deploy dashy into.

#### Default value:

```yaml
kubectl_dashy_namespace: monitoring
```

## tsolo.kubernetes/kubectl/homepage

Install Homepage

### kubectl_homepage_hostname

The hostname for Homepage.

#### Default value:

```yaml
kubectl_homepage_hostname: homepage-martin.sansa.org.za
```

## tsolo.kubernetes/kubectl/skooner

Install Skooner, a light Kubernetes Dashboard

### artifacts_dir

Where the kubernetes config files are placed. This is a path on the host
that Ansible run on.

#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### kubectl_skooner_hostname

The hostname for skooner.

#### Default value:

```yaml
kubectl_skooner_hostname: skooner.{{ internal_domain }}
```

## tsolo.kubernetes/olm/install



### olm_version


#### Default value:

```yaml
olm_version: v0.20.0
```

## tsolo.kubernetes/olm/pgo



### olm_pgo_backup_storage_class

Name of the storage class to use when creating PVC for backups.

#### Default value:

```yaml
olm_pgo_backup_storage_class: nfs-csi
```

### olm_pgo_backup_storage_size

Requested size of backup volume. Not all storage classes will obey this
request. String with size and unit e.g. 100Gi, 1Ti.

#### Default value:

```yaml
olm_pgo_backup_storage_size: 40Gi
```

### olm_pgo_database_storage_class

Name of the storage class to use when creating PVC for database.

#### Default value:

```yaml
olm_pgo_database_storage_class: nfs-csi
```

### olm_pgo_database_storage_size

Requested size of database volume. Not all storage classes will obey this
request. String with size and unit e.g. 100Gi, 1Ti.

#### Default value:

```yaml
olm_pgo_database_storage_size: 40Gi
```

### olm_pgo_users

A list of Users and Databases to create when the Cluster is instantiated.

#### Default value:

```yaml
olm_pgo_users:
- databases:
  - test
  name: test
  options: NOSUPERUSER
```

## tsolo.kubernetes/sonobuoy



### sonobuoy_version


#### Default value:

```yaml
sonobuoy_version: v0.55.0
```

## tsolo.kubernetes/startpage



### startpage_hostname

Hostname to use for Dashy.

#### Default value:

```yaml
startpage_hostname: start.{{ internal_domain }}
```

### startpage_namespace

Namespace to deploy dashy into.

#### Default value:

```yaml
startpage_namespace: monitoring
```