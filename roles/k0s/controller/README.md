# tsolo.kubernetes/k0s/controller

Install K0s controller

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

Where the kubernetes config files are placed. This is a path on the host that Ansible run on.

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
