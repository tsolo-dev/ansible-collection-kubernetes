# roles/k0s/controller/tasks/start_service.yaml
---
- name: Check if k0s controller service exists
  ansible.builtin.stat:
    path: /etc/systemd/system/k0scontroller.service
  register: k0scontroller_service

- name: Create k0s command
  ansible.builtin.set_fact:
    k0s_argv:
      - /usr/local/bin/k0s
      - install
      - controller
      - --config
      - /etc/k0s/k0s.yaml

- name: Set as worker
  ansible.builtin.set_fact:
    k0s_argv: "{{ k0s_argv + ['--enable-worker'] }}"
  when: inventory_hostname in groups['k8s_workers']

- name: Set token file
  ansible.builtin.set_fact:
    k0s_argv: "{{ k0s_argv + ['--token-file', _controller_token_filename] }}"
  when: _controller_token_filename is defined and _controller_token_filename

- name: Create k0s controller service with install command
  ansible.builtin.command:
    argv: "{{ k0s_argv }}"
  register: install_controller_cmd
  changed_when: install_controller_cmd | length > 0
  when: not k0scontroller_service.stat.exists

- name: Enable and check k0s service
  # Restart only one controller at a time
  ansible.builtin.systemd:
    name: k0scontroller
    state: restarted
    daemon_reload: "{{ not k0scontroller_service.stat.exists }}"
    enabled: true
  throttle: 1
  timeout: 60

- name: Wait for k8s apiserver
  # Dont fail on this task, we just wait for the service
  # to start. Use k0s status to check if service is functional.
  ansible.builtin.wait_for:
    host: localhost
    port: 6443
    delay: 15
    timeout: 90
  failed_when: false

- name: Get K0s status directly
  # Dont fail on this task the next task block will check the output.
  ansible.builtin.command:
    cmd: /usr/local/bin/k0s status
  register: _k0s_command_status
  failed_when: false
  changed_when: false

- name: Additional task if controller did not start
  when: _k0s_command_status.rc != 0
  block:
    - name: Enable and check k0s service
      ansible.builtin.systemd:
        name: k0scontroller
        state: stopped
    - name: Remove socket file
      # Could check if _k0s_command_status contains
      # '''Error: Get "http://localhost": dial unix
      # /run/k0s/status.sock: connect: connection refused''',
      # regardless the socket file should not be there when
      # the service is stopped.
      ansible.builtin.file:
        path: /run/k0s/status.sock
        state: absent
    - name: Enable and check K0s service
      ansible.builtin.systemd:
        name: k0scontroller
        state: started
      timeout: 60
    - name: Wait for k8s apiserver
      # Wait longer and fail if port is not open.
      ansible.builtin.wait_for:
        host: localhost
        port: 6443
        delay: 15
        timeout: 180
    - name: Get K0s status directly
      ansible.builtin.command:
        cmd: /usr/local/bin/k0s status
