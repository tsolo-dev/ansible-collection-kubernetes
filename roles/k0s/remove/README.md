# tsolo.kubernetes/k0s/remove

## Table of content

- [Default Variables](#default-variables)
  - [destroy_k0s](#destroy_k0s)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### destroy_k0s

#### Default value

```YAML
destroy_k0s: false
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
