# tsolo.kubernetes/k0s/install

Install Kubernetes using K0s. K0s is a light pre-package and tested Kubernetes release.
If */data* exists and is a mounted filesystem then */data/k0s* will be used as the working directory for Kubernetes. The default path (*/var/lib/k0s*) will be an symbolic link to */data/k0s*. Similarly if */data* exists and is a mounted filesystem then */data/openebs* will be used as the working directory for OpenEBS. The default path (*/var/openebs*) will be an symbolic link to */data/openebs*. K0s places the kubelet working directory at */var/lib/k0s/kubelet*, there are some libraries that do not allow the kubelet directory to be set. A symbolic link from */var/lib/k0s/kubelet* to */var/lib/kubelet* is created.

## Table of content

- [Default Variables](#default-variables)
  - [k0s_version](#k0s_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### k0s_version

The version of K0s to install.

#### Default value

```YAML
k0s_version: v1.22.9+k0s.0
```



## Dependencies

- ordereddict([('role', 'common')])

## License

Apache-2.0

## Author

Tsolo.io
