# tsolo.kubernetes/k0s/ha

Setup high availability (HA) access to the controllers. Participating
worker nodes proxy controller access and share a floating IP.


## Table of content

- [Default Variables](#default-variables)
  - [k0s_ha_auth_password](#k0s_ha_auth_password)
  - [k0s_ha_cidr](#k0s_ha_cidr)
  - [k0s_ha_participation_enabled](#k0s_ha_participation_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### k0s_ha_auth_password

Shared password used to authenticate HA Keepalive nodes to one another.

#### Default value

```YAML
k0s_ha_auth_password: ryr3S2s0s
```

### k0s_ha_cidr

The IP address and netmask in CIDR format to use as the floating address for high availability (HA). This address will be assigned to one of the worker nodes.

#### Default value

```YAML
k0s_ha_cidr: ''
```

#### Example usage

```YAML
k0s_ha_cidr: 192.168.11.30/24
```

### k0s_ha_participation_enabled

If a node should participate in the HA cluster. This variable is only set at group and host vars level. It will result in the host not being used for HA purposes. If there are less than 3 eligible nodes the HA deployment will fail.

#### Default value

```YAML
k0s_ha_participation_enabled: true
```



## Dependencies

- ordereddict([('role', 'tsolo.node.haproxy')])

## License

Apache-2.0

## Author

Tsolo.io
