# tsolo.kubernetes/k0s/upgrade

Upgrade the k0s installation.

## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
