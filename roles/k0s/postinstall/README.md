# tsolo.kubernetes/k0s/postinstall

Tasks to perform after Kubernetes has been installed.

## Table of content

- [Default Variables](#default-variables)
  - [k0s_node_labels](#k0s_node_labels)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### k0s_node_labels

k0s_node_labels is a dictionary, the key value pairs will be applied as labels in Kubernetes. Each node can have a k0s_node_labels defined or normal Ansible variable definitions per group can be done.

#### Default value

```YAML
k0s_node_labels: {}
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
