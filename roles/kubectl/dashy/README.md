# tsolo.kubernetes/kubectl/dashy

## Table of content

- [Default Variables](#default-variables)
  - [kubectl_dashy_docker_image](#kubectl_dashy_docker_image)
  - [kubectl_dashy_hostname](#kubectl_dashy_hostname)
  - [kubectl_dashy_namespace](#kubectl_dashy_namespace)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### kubectl_dashy_docker_image

The docker image to use for Dashy.

#### Default value

```YAML
kubectl_dashy_docker_image: lissy93/dashy:latest
```

### kubectl_dashy_hostname

Hostname to use for Dashy.

#### Default value

```YAML
kubectl_dashy_hostname: dashy.{{ internal_domain }}
```

### kubectl_dashy_namespace

Namespace to deploy dashy into.

#### Default value

```YAML
kubectl_dashy_namespace: monitoring
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
