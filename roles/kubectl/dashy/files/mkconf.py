#!/usr/bin/env python3

from collections import defaultdict
from pathlib import Path

from yaml import dump, load

try:
    from yaml import CDumper as Dumper
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Dumper, Loader

if __name__ == "__main__":
    services = defaultdict(dict)
    for filepath in Path("/opt/kubectl/dashy/services.d").iterdir():
        service = load(filepath.read_text(), Loader=Loader)
        section = service["__section"]
        name = service["name"]
        del service["name"]
        service["__name"] = name
        service["title"] = service.get("title", name)
        services[section][name] = service
        for key in list(service.keys()):
            if key.startswith("__"):
                del services[section][name][key]

    section_list = []
    for section_name in sorted(services):
        section = {"icon": "fas fa-rocket", "name": section_name, "items": []}
        for service_name in sorted(services[section_name]):
            section["items"].append(services[section_name][service_name])
        section_list.append(section)
    Path("/opt/kubectl/dashy/values.d/services.yaml").write_text(dump({"sections": section_list}))
