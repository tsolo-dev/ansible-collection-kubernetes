# tsolo.kubernetes/kubectl/skooner

Install Skooner, a light Kubernetes Dashboard

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
  - [kubectl_skooner_hostname](#kubectl_skooner_hostname)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

Where the kubernetes config files are placed. This is a path on the host that Ansible run on.

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### kubectl_skooner_hostname

The hostname for skooner.

#### Default value

```YAML
kubectl_skooner_hostname: skooner.{{ internal_domain }}
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
