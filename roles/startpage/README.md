# tsolo.kubernetes/startpage

## Table of content

- [Default Variables](#default-variables)
  - [startpage_hostname](#startpage_hostname)
  - [startpage_namespace](#startpage_namespace)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### startpage_hostname

Hostname to use for Dashy.

#### Default value

```YAML
startpage_hostname: start.{{ internal_domain }}
```

### startpage_namespace

Namespace to deploy dashy into.

#### Default value

```YAML
startpage_namespace: monitoring
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
