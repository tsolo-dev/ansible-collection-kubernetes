# tsolo.kubernetes/olm/install

## Table of content

- [Default Variables](#default-variables)
  - [olm_version](#olm_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### olm_version

#### Default value

```YAML
olm_version: v0.20.0
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
