# tsolo.kubernetes/olm/pgo

## Table of content

- [Default Variables](#default-variables)
  - [olm_pgo_backup_storage_class](#olm_pgo_backup_storage_class)
  - [olm_pgo_backup_storage_size](#olm_pgo_backup_storage_size)
  - [olm_pgo_database_storage_class](#olm_pgo_database_storage_class)
  - [olm_pgo_database_storage_size](#olm_pgo_database_storage_size)
  - [olm_pgo_users](#olm_pgo_users)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### olm_pgo_backup_storage_class

Name of the storage class to use when creating PVC for backups.

#### Default value

```YAML
olm_pgo_backup_storage_class: nfs-csi
```

### olm_pgo_backup_storage_size

Requested size of backup volume. Not all storage classes will obey this request. String with size and unit e.g. 100Gi, 1Ti.

#### Default value

```YAML
olm_pgo_backup_storage_size: 40Gi
```

### olm_pgo_database_storage_class

Name of the storage class to use when creating PVC for database.

#### Default value

```YAML
olm_pgo_database_storage_class: nfs-csi
```

### olm_pgo_database_storage_size

Requested size of database volume. Not all storage classes will obey this request. String with size and unit e.g. 100Gi, 1Ti.

#### Default value

```YAML
olm_pgo_database_storage_size: 40Gi
```

### olm_pgo_users

A list of Users and Databases to create when the Cluster is instantiated.

#### Default value

```YAML
olm_pgo_users:
  - name: test
    databases:
      - test
    options: NOSUPERUSER
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
