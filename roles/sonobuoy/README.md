# tsolo.kubernetes/sonobuoy

## Table of content

- [Default Variables](#default-variables)
  - [sonobuoy_version](#sonobuoy_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### sonobuoy_version

#### Default value

```YAML
sonobuoy_version: v0.55.0
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
