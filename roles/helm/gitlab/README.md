# tsolo.kubernetes/helm/gitlab

Install GitLab on Kubernetes. This role is still work in progress (WIP).

## Table of content

- [Default Variables](#default-variables)
  - [helm_gitlab_chart_version](#helm_gitlab_chart_version)
  - [helm_gitlab_hostname](#helm_gitlab_hostname)
  - [helm_gitlab_storage_class](#helm_gitlab_storage_class)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_gitlab_chart_version

The version of the GitLab chart that will be installed.

#### Default value

```YAML
helm_gitlab_chart_version: 4.2.6
```

### helm_gitlab_hostname

The hostname of the GitLab service.

#### Default value

```YAML
helm_gitlab_hostname: gitlab.{{ internal_domain }}
```

### helm_gitlab_storage_class

The storage class to use.

#### Default value

```YAML
helm_gitlab_storage_class: nfs-csi
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
