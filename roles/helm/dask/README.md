# tsolo.kubernetes/helm/dask

Install Dask

## Table of content

- [Default Variables](#default-variables)
  - [helm_dask_chart_version](#helm_dask_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_dask_chart_version

Version of Dask Chart to install.

#### Default value

```YAML
helm_dask_chart_version: 2022.6.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
