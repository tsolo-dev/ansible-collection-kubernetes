# roles/helm/prometheus/tasks/external-exporter.yaml
---
- name: Set internal variables
  ansible.builtin.set_fact:
    _name: "{{ external.name }}"
    _addresses: "{{ external.addresses | default([]) }}"
    _group: "{{ external.group | default('') }}"
    _port: "{{ external.port | default(80) | int }}"
    _path: "{{ external.path | default('/metrics') }}"
    _interval: "{{ external.interval | default(30) }}"

- name: Check that either group or addresses are given.
  ansible.builtin.fail:
    msg: Addresses and/or group need to be supplied.
  when: not (_addresses or _group)

- name: Define template target file
  ansible.builtin.set_fact:
    _endpoint_file: /opt/k0s/prometheus-exporters/external-{{ _name }}-exporter-endpoints.yaml

- name: Write Endpoints template
  ansible.builtin.template:
    src: external-exporter-endpoints.yaml.j2
    dest: "{{ _endpoint_file }}"

- name: Load Endpoints object with addresses of {{ _name }}
  ansible.builtin.command:
    cmd: kubectl apply -f {{ _endpoint_file }}

- name: Services object for Prometheus to discover {{ _name }}
  kubernetes.core.k8s:
    state: present
    definition:
      kind: Service
      apiVersion: v1
      metadata:
        name: external-{{ _name }}-exporter
        namespace: monitoring
        labels:
          app: external-{{ _name }}-exporter
      spec:
        type: ClusterIP
        ports:
          - name: http-metrics
            port: "{{ _port | int }}"
            targetPort: "{{ _port | int }}"

- name: Create a ServiceMonitor for Prometheus to find external {{ _name }}
  kubernetes.core.k8s:
    state: present
    definition:
      apiVersion: monitoring.coreos.com/v1
      kind: ServiceMonitor
      metadata:
        name: external-{{ _name }}-exporter
        namespace: monitoring
        labels:
          release: kube-prometheus-stack
      spec:
        endpoints:
          - path: "{{ _path }}"
            interval: "{{ _interval }}s"
            port: http-metrics
        namespaceSelector:
          matchNames:
            - monitoring
        selector:
          matchLabels:
            app: external-{{ _name }}-exporter
  failed_when: false
