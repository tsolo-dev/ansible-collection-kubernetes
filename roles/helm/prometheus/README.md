# tsolo.kubernetes/helm/prometheus

Install and configure Prometheus.

## Table of content

- [Default Variables](#default-variables)
  - [helm_prometheus_alertmanager_hostname](#helm_prometheus_alertmanager_hostname)
  - [helm_prometheus_chart_version](#helm_prometheus_chart_version)
  - [helm_prometheus_hostname](#helm_prometheus_hostname)
  - [helm_prometheus_retention](#helm_prometheus_retention)
  - [helm_prometheus_retention_size](#helm_prometheus_retention_size)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_prometheus_alertmanager_hostname

Hostname to use for Prometheus Alertmanager service.

#### Default value

```YAML
helm_prometheus_alertmanager_hostname: alertmanager.{{ internal_domain }}
```

### helm_prometheus_chart_version

Version of Prometheus chart to be installed using Helm.

#### Default value

```YAML
helm_prometheus_chart_version: 35.0.2
```

### helm_prometheus_hostname

Hostname to use for prometheus service.

#### Default value

```YAML
helm_prometheus_hostname: prometheus.{{ internal_domain }}
```

### helm_prometheus_retention

How long to retain metrics. When to remove old data. Number of days.

#### Default value

```YAML
helm_prometheus_retention: 32
```

### helm_prometheus_retention_size

Maximum size of metrics. The maximum number of bytes of storage blocks to retain. The oldest data will be removed first. Defaults to 0 or disabled. Units supported: B, KB, MB, GB, TB, PB, EB. Ex: "512MB". Based on powers-of-2, so 1KB is 1024B. Only the persistent blocks are deleted to honor this retention although WAL and m-mapped chunks are counted in the total size. So the minimum requirement for the disk is the peak space taken by the wal (the WAL and Checkpoint) and chunks_head (m-mapped Head chunks) directory combined (peaks every 2 hours).

#### Default value

```YAML
helm_prometheus_retention_size: 1TB
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
