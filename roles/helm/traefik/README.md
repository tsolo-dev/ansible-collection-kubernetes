# tsolo.kubernetes/helm/traefik

Install Traefik proxy service.

## Table of content

- [Default Variables](#default-variables)
  - [helm_traefik_chart_version](#helm_traefik_chart_version)
  - [helm_traefik_dashboard_hostname](#helm_traefik_dashboard_hostname)
  - [helm_traefik_dashboard_password](#helm_traefik_dashboard_password)
  - [helm_traefik_load_balancer_ip](#helm_traefik_load_balancer_ip)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_traefik_chart_version

Traefik Chart version to install.

#### Default value

```YAML
helm_traefik_chart_version: 10.21.1
```

### helm_traefik_dashboard_hostname

Hostname of Traefik dashboard.

#### Default value

```YAML
helm_traefik_dashboard_hostname: traefik.{{ internal_domain }}
```

### helm_traefik_dashboard_password

Password to traefik dashboard.

#### Default value

```YAML
helm_traefik_dashboard_password: password
```

### helm_traefik_load_balancer_ip

The IP address that Treafik will listen on. If not defined Loadbalaner will automaticaly asign address.

#### Default value

```YAML
helm_traefik_load_balancer_ip: 192.168.1.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
