# tsolo.kubernetes/helm/loki

Install Loki the Log aggregation service from Grafana.

## Table of content

- [Default Variables](#default-variables)
  - [helm_loki_chart_version](#helm_loki_chart_version)
  - [helm_loki_object_storage](#helm_loki_object_storage)
  - [helm_loki_retention_hours](#helm_loki_retention_hours)
  - [helm_loki_storage](#helm_loki_storage)
  - [helm_promtail_chart_version](#helm_promtail_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_loki_chart_version

The version of the Loki chart to use.

#### Default value

```YAML
helm_loki_chart_version: 2.13.2
```

### helm_loki_object_storage

The S3 storage to use for Loki. The format for this connection string is

### helm_loki_retention_hours

The period in hours that logs should be kept. Loks older than this period will be deleted at the next clean up cycle.

#### Default value

```YAML
helm_loki_retention_hours: 72
```

### helm_loki_storage

#### Default value

```YAML
helm_loki_storage: ''
```

### helm_promtail_chart_version

The version of the Promtail chart to use.

#### Default value

```YAML
helm_promtail_chart_version: 6.2.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
