# tsolo.kubernetes/helm/postgresql_ha

## Table of content

- [Default Variables](#default-variables)
  - [helm_postgresql_address](#helm_postgresql_address)
  - [helm_postgresql_admin_password](#helm_postgresql_admin_password)
  - [helm_postgresql_ha_chart_version](#helm_postgresql_ha_chart_version)
  - [helm_postgresql_password](#helm_postgresql_password)
  - [helm_postgresql_repmgrPassword](#helm_postgresql_repmgrPassword)
  - [helm_postgresql_storage_class](#helm_postgresql_storage_class)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_postgresql_address

#### Default value

```YAML
helm_postgresql_address: ''
```

### helm_postgresql_admin_password

#### Default value

```YAML
helm_postgresql_admin_password: eawzVE0fX5
```

### helm_postgresql_ha_chart_version

#### Default value

```YAML
helm_postgresql_ha_chart_version: 8.2.7
```

### helm_postgresql_password

#### Default value

```YAML
helm_postgresql_password: secret
```

### helm_postgresql_repmgrPassword

#### Default value

```YAML
helm_postgresql_repmgrPassword: cOaFLtaSt1
```

### helm_postgresql_storage_class

#### Default value

```YAML
helm_postgresql_storage_class: local-hostpath
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
