# tsolo.kubernetes/helm/postgresql_single

## Table of content

- [Default Variables](#default-variables)
  - [helm_postgresql_single_chart_version](#helm_postgresql_single_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_postgresql_single_chart_version

#### Default value

```YAML
helm_postgresql_single_chart_version: 10.13.14
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
