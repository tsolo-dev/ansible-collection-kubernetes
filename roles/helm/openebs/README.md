# tsolo.kubernetes/helm/openebs

Install OpenEBS storage server using Helm.

## Table of content

- [Default Variables](#default-variables)
  - [helm_openebs_chart_version](#helm_openebs_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_openebs_chart_version

The version of OpenEBS chart that will be installed.

#### Default value

```YAML
helm_openebs_chart_version: 3.1.0
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
