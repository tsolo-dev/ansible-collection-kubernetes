# tsolo.kubernetes/helm/grafana

Install Grafana the metrics visiulisation platform.

## Table of content

- [Default Variables](#default-variables)
  - [grafana_keycloak_client_token](#grafana_keycloak_client_token)
  - [helm_grafana_admin_password](#helm_grafana_admin_password)
  - [helm_grafana_chart_version](#helm_grafana_chart_version)
  - [helm_grafana_hostname](#helm_grafana_hostname)
  - [helm_grafana_tls_secret](#helm_grafana_tls_secret)
  - [helm_keycloak_hostname](#helm_keycloak_hostname)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### grafana_keycloak_client_token

Shared secret between Keycloak and Grafana.

#### Default value

```YAML
grafana_keycloak_client_token: 5c9ad8e43c65255a0feecfacf420bac57d6c300f71b9e850ebe423b31881dda5
```

### helm_grafana_admin_password

The default admin password.

#### Default value

```YAML
helm_grafana_admin_password: admin
```

### helm_grafana_chart_version

The version of Grafana chart to use. https://github.com/grafana/helm-charts/tags

#### Default value

```YAML
helm_grafana_chart_version: 6.29.6
```

### helm_grafana_hostname

Hostname to use for Grafana.

#### Default value

```YAML
helm_grafana_hostname: grafana.{{ internal_domain }}
```

### helm_grafana_tls_secret

Name of the secret object with TLS certificate and key. If not defined or empty then Grafana will be deployed with HTTP not HTTPS.

#### Default value

```YAML
helm_grafana_tls_secret: ''
```

### helm_keycloak_hostname

The hostname of Keycloak. See helm/keycloak role. If not defined or empty then no external authenticator is used.

#### Default value

```YAML
helm_keycloak_hostname: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
