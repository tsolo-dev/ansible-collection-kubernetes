# tsolo.kubernetes/helm/nginx

Install Nginx controller for ingest.

## Table of content

- [Default Variables](#default-variables)
  - [helm_nginx_chart_version](#helm_nginx_chart_version)
  - [helm_nginx_configmap](#helm_nginx_configmap)
  - [helm_nginx_load_balancer_ip](#helm_nginx_load_balancer_ip)
  - [helm_nginx_oauth_client_token](#helm_nginx_oauth_client_token)
  - [helm_nginx_vouch_domain](#helm_nginx_vouch_domain)
  - [helm_nginx_vouch_enabled](#helm_nginx_vouch_enabled)
  - [helm_nginx_vouch_hostname](#helm_nginx_vouch_hostname)
  - [helm_nginx_vouch_jwt_token](#helm_nginx_vouch_jwt_token)
  - [helm_nginx_vouch_oauth_client_token](#helm_nginx_vouch_oauth_client_token)
  - [helm_nginx_vouch_tls_secret](#helm_nginx_vouch_tls_secret)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_nginx_chart_version

Nginx controller chart version to install.

#### Default value

```YAML
helm_nginx_chart_version: 9.2.11
```

### helm_nginx_configmap

Configure the Ingest controller. According to https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap Changes to config map requires a restart to the Nginx controller.

#### Default value

```YAML
helm_nginx_configmap:
  proxy-body-size: 100m
  proxy-buffer-size: 256k
  proxy-buffering: on
  proxy-buffers-number: '4'
  proxy-max-temp-file-size: 1024m
```

### helm_nginx_load_balancer_ip

The IP address that Treafik will listen on. If not defined Loadbalaner will automaticaly asign address.

#### Default value

```YAML
helm_nginx_load_balancer_ip: 192.168.1.1
```

### helm_nginx_oauth_client_token

#### Default value

```YAML
helm_nginx_oauth_client_token: ssssss
```

### helm_nginx_vouch_domain

The domain Vouch will provide SSO for.

#### Default value

```YAML
helm_nginx_vouch_domain: '{{ internal_domain }}'
```

### helm_nginx_vouch_enabled

If vouch should be installed with Nginx. Not implemented Yet. Vouch is always installed at the moment.

#### Default value

```YAML
helm_nginx_vouch_enabled: false
```

### helm_nginx_vouch_hostname

The hostname of the vouch service.

#### Default value

```YAML
helm_nginx_vouch_hostname: vouch.{{ internal_domain }}
```

### helm_nginx_vouch_jwt_token

A token string used in JWT generation

#### Default value

```YAML
helm_nginx_vouch_jwt_token: a238483487234623847264826348276348263482736482736482736428f
```

### helm_nginx_vouch_oauth_client_token

The OAuth client_secret used by Vouch.

#### Default value

```YAML
helm_nginx_vouch_oauth_client_token: ''
```

### helm_nginx_vouch_tls_secret

The name of the Kubernetes Secret with the TLS certificates and key.

#### Default value

```YAML
helm_nginx_vouch_tls_secret: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
