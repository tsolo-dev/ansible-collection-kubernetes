# tsolo.kubernetes/helm/mimir

Install Mimir the metrics storage service from Grafana.

## Table of content

- [Default Variables](#default-variables)
  - [helm_mimir_chart_version](#helm_mimir_chart_version)
  - [helm_mimir_retention_days](#helm_mimir_retention_days)
  - [helm_mimir_s3_access_key](#helm_mimir_s3_access_key)
  - [helm_mimir_s3_alerts_bucket](#helm_mimir_s3_alerts_bucket)
  - [helm_mimir_s3_blocks_bucket](#helm_mimir_s3_blocks_bucket)
  - [helm_mimir_s3_host](#helm_mimir_s3_host)
  - [helm_mimir_s3_https_enabled](#helm_mimir_s3_https_enabled)
  - [helm_mimir_s3_ruler_bucket](#helm_mimir_s3_ruler_bucket)
  - [helm_mimir_s3_rules_bucket](#helm_mimir_s3_rules_bucket)
  - [helm_mimir_s3_secret_key](#helm_mimir_s3_secret_key)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_mimir_chart_version

The version of the Mimir chart to use.

#### Default value

```YAML
helm_mimir_chart_version: 3.0.0
```

### helm_mimir_retention_days

The period in days that metrics should be kept. Metrics older than this period will be deleted at the next clean up cycle.

#### Default value

```YAML
helm_mimir_retention_days: 370
```

### helm_mimir_s3_access_key

#### Default value

```YAML
helm_mimir_s3_access_key: mimir
```

### helm_mimir_s3_alerts_bucket

#### Default value

```YAML
helm_mimir_s3_alerts_bucket: mimir-alerts
```

### helm_mimir_s3_blocks_bucket

#### Default value

```YAML
helm_mimir_s3_blocks_bucket: mimir-blocks
```

### helm_mimir_s3_host

#### Default value

```YAML
helm_mimir_s3_host: 192.168.1.1:9000
```

### helm_mimir_s3_https_enabled

#### Default value

```YAML
helm_mimir_s3_https_enabled: false
```

### helm_mimir_s3_ruler_bucket

### helm_mimir_s3_rules_bucket

#### Default value

```YAML
helm_mimir_s3_rules_bucket: mimir-rules
```

### helm_mimir_s3_secret_key

#### Default value

```YAML
helm_mimir_s3_secret_key: Ecvjdhfiuehfn3q84ddwekdno23
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
