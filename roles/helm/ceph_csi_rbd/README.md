# tsolo.kubernetes/helm/ceph_csi_rbd

## Table of content

- [Default Variables](#default-variables)
  - [helm_ceph_csi_rbd_chart_version](#helm_ceph_csi_rbd_chart_version)
  - [helm_ceph_csi_rbd_key](#helm_ceph_csi_rbd_key)
  - [helm_ceph_csi_rbd_mons](#helm_ceph_csi_rbd_mons)
  - [helm_ceph_csi_rbd_pool](#helm_ceph_csi_rbd_pool)
  - [helm_ceph_csi_rbd_user](#helm_ceph_csi_rbd_user)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_ceph_csi_rbd_chart_version

#### Default value

```YAML
helm_ceph_csi_rbd_chart_version: 3.4.0
```

### helm_ceph_csi_rbd_key

#### Default value

```YAML
helm_ceph_csi_rbd_key: xyz
```

### helm_ceph_csi_rbd_mons

#### Default value

```YAML
helm_ceph_csi_rbd_mons:
  - 10.0.0.1:6789
  - 10.0.0.2:6789
  - 10.0.0.3:6789
```

### helm_ceph_csi_rbd_pool

#### Default value

```YAML
helm_ceph_csi_rbd_pool: rbd
```

### helm_ceph_csi_rbd_user

#### Default value

```YAML
helm_ceph_csi_rbd_user: admin
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
