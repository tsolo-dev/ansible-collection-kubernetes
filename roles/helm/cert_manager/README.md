# tsolo.kubernetes/helm/cert_manager

Install and Configure Cert-Manager

## Table of content

- [Default Variables](#default-variables)
  - [helm_cert_manager_chart_version](#helm_cert_manager_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_cert_manager_chart_version

The version of the Cert-Manager chart that will be installed.

#### Default value

```YAML
helm_cert_manager_chart_version: v1.8.2
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
