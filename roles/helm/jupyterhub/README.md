# tsolo.kubernetes/helm/jupyterhub

Install and configure JupyterHub

## Table of content

- [Default Variables](#default-variables)
  - [helm_jupyterhub_chart_version](#helm_jupyterhub_chart_version)
  - [helm_jupyterhub_dask_gateway_api_token](#helm_jupyterhub_dask_gateway_api_token)
  - [helm_jupyterhub_extra_config](#helm_jupyterhub_extra_config)
  - [helm_jupyterhub_hostname](#helm_jupyterhub_hostname)
  - [helm_jupyterhub_hub_postgresql](#helm_jupyterhub_hub_postgresql)
  - [helm_jupyterhub_profileList](#helm_jupyterhub_profileList)
  - [helm_jupyterhub_proxy_address](#helm_jupyterhub_proxy_address)
  - [helm_jupyterhub_shutdown_idle_hours](#helm_jupyterhub_shutdown_idle_hours)
  - [helm_jupyterhub_singleuser_storage_class](#helm_jupyterhub_singleuser_storage_class)
  - [helm_jupyterhub_tls_secret](#helm_jupyterhub_tls_secret)
  - [helm_jupyterhub_user_share_pvc](#helm_jupyterhub_user_share_pvc)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_jupyterhub_chart_version

Version of the JupyterHub Helm chart to use.

#### Default value

```YAML
helm_jupyterhub_chart_version: 1.2.0
```

### helm_jupyterhub_dask_gateway_api_token

#### Default value

```YAML
helm_jupyterhub_dask_gateway_api_token: ''
```

### helm_jupyterhub_extra_config

Additional configuration to pass to JupyterHub. This value is assigned to *hub.extraConfig* in the JupyterHub Chart. The contents is converted to YAML when applied, thus .

#### Default value

```YAML
helm_jupyterhub_extra_config: {}
```

### helm_jupyterhub_hostname

#### Default value

```YAML
helm_jupyterhub_hostname: jupyterhub.{{ internal_domain }}
```

### helm_jupyterhub_hub_postgresql

Database credentials. The database used by the Hub.

#### Default value

```YAML
helm_jupyterhub_hub_postgresql:
  database: jupyterhub
  hostname: postgresql-ha-pgpool.database.svc.cluster.local
  password: jupyterhub
  port: 5432
  username: jupyterhub
```

### helm_jupyterhub_profileList

#### Default value

```YAML
helm_jupyterhub_profileList:
  - default: true
    description: Python, R, and Julia.
    display_name: Datascience environment
    kubespawner_override:
      image: jupyter/datascience-notebook:python-3.9.7
```

### helm_jupyterhub_proxy_address

The IP address to assign to the JupyterHub loadbalancer proxy.

#### Default value

```YAML
helm_jupyterhub_proxy_address: ''
```

### helm_jupyterhub_shutdown_idle_hours

Single user instances are shutdown after they have been idle for sometime. The cull process will stop images if they are inactive for a length of time. This value is in hours and is converted to second by the deployment scripts. See https://github.com/jupyterhub/jupyterhub-idle-culler#how-it-works for more details regarding how the cull process works.

#### Default value

```YAML
helm_jupyterhub_shutdown_idle_hours: 3
```

### helm_jupyterhub_singleuser_storage_class

The storage Class to use for single user instances

#### Default value

```YAML
helm_jupyterhub_singleuser_storage_class: nfs-csi
```

### helm_jupyterhub_tls_secret

The name of the Kubernetes secret in the same namespace that will be used to setup HTTPS. The secret should contain tls.crt and tls.key fileds for the certificate and keys. If not set HTTP is configured.

#### Default value

```YAML
helm_jupyterhub_tls_secret: ''
```

### helm_jupyterhub_user_share_pvc

An existing Persistent Volume Claim (PVC) that will be mounted into every Jupyter container. This is to share data between users.

#### Default value

```YAML
helm_jupyterhub_user_share_pvc:
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
