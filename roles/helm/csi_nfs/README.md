# tsolo.kubernetes/helm/csi_nfs

Install and configure the NFS CSI driver.

## Table of content

- [Default Variables](#default-variables)
  - [helm_csi_nfs_chart_version](#helm_csi_nfs_chart_version)
  - [helm_csi_nfs_storage_class_default_enabled](#helm_csi_nfs_storage_class_default_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_csi_nfs_chart_version

The version of the Helm Chart that will be installed.

#### Default value

```YAML
helm_csi_nfs_chart_version: v4.1.0
```

### helm_csi_nfs_storage_class_default_enabled

#### Default value

```YAML
helm_csi_nfs_storage_class_default_enabled: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
