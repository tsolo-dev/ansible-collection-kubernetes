# tsolo.kubernetes/helm/install

## Table of content

- [Default Variables](#default-variables)
  - [helm_version](#helm_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_version

The version of Helm to use. Only versions prepackaged version from https://quay.io/repository/tsolo/helm?tab=tags can be selected.

#### Default value

```YAML
helm_version: v3.9.0
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
