# tsolo.kubernetes/helm/keycloak

## Table of content

- [Default Variables](#default-variables)
  - [helm_keycloak_address](#helm_keycloak_address)
  - [helm_keycloak_admin_password](#helm_keycloak_admin_password)
  - [helm_keycloak_chart_version](#helm_keycloak_chart_version)
  - [helm_keycloak_hostname](#helm_keycloak_hostname)
  - [helm_keycloak_ingress_class](#helm_keycloak_ingress_class)
  - [helm_keycloak_management_password](#helm_keycloak_management_password)
  - [helm_keycloak_namespace](#helm_keycloak_namespace)
  - [helm_keycloak_postgresql](#helm_keycloak_postgresql)
  - [helm_keycloak_tls_secret](#helm_keycloak_tls_secret)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_keycloak_address

The address assosiated with the running instance of Keycloak.

#### Default value

```YAML
helm_keycloak_address: ''
```

### helm_keycloak_admin_password

The admin password for Keycloak.

#### Default value

```YAML
helm_keycloak_admin_password: password
```

### helm_keycloak_chart_version

The version of the Keycloak chart to install.

#### Default value

```YAML
helm_keycloak_chart_version: 9.2.11
```

### helm_keycloak_hostname

The hostname that will be used for Keycloak.

#### Default value

```YAML
helm_keycloak_hostname: keycloak.{{ internal_domain }}
```

### helm_keycloak_ingress_class

Name of the ingress class to use. Not used if helm_keycloak_address is set.

#### Default value

```YAML
helm_keycloak_ingress_class: traefik
```

### helm_keycloak_management_password

The management password for Keycloak.

#### Default value

```YAML
helm_keycloak_management_password: password
```

### helm_keycloak_namespace

Namespace to install Keycloak into.

#### Default value

```YAML
helm_keycloak_namespace: identitymanagement
```

### helm_keycloak_postgresql

Database credentials for Keycloak.

#### Default value

```YAML
helm_keycloak_postgresql:
  database: keycloak
  hostname: database
  password:
  port: 5432
  username: keycloak
```

### helm_keycloak_tls_secret

Secret with TLS key and certificate Not used if helm_keycloak_address is set.

#### Default value

```YAML
helm_keycloak_tls_secret: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
