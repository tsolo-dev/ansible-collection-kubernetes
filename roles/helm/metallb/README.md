# tsolo.kubernetes/helm/metallb

Install Metal Load Balancer (MetalLB)

## Table of content

- [Default Variables](#default-variables)
  - [helm_metallb_address_range](#helm_metallb_address_range)
  - [helm_metallb_chart_version](#helm_metallb_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_metallb_address_range

#### Default value

```YAML
helm_metallb_address_range: 10.0.0.100-10.0.0.200
```

### helm_metallb_chart_version

The version of the MetalLB chart that will be installed.

#### Default value

```YAML
helm_metallb_chart_version: 0.13.9
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
