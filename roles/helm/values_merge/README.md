# tsolo.kubernetes/helm/values_merge

## Table of content

- [Default Variables](#default-variables)
  - [mergedir](#mergedir)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### mergedir

#### Default value

```YAML
mergedir: /tmp
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
