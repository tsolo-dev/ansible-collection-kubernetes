# tsolo.kubernetes/helm/jenkins

Install Jenkins CI

## Table of content

- [Default Variables](#default-variables)
  - [helm_jenkins_chart_version](#helm_jenkins_chart_version)
  - [helm_jenkins_hostname](#helm_jenkins_hostname)
  - [helm_jenkins_storage_class](#helm_jenkins_storage_class)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_jenkins_chart_version

The version of the Jenkins chart that will be installed.

#### Default value

```YAML
helm_jenkins_chart_version: 4.2.8
```

### helm_jenkins_hostname

The hostname of the jenkins service.

#### Default value

```YAML
helm_jenkins_hostname: jenkins.{{ internal_domain }}
```

### helm_jenkins_storage_class

The storage class to use.

#### Default value

```YAML
helm_jenkins_storage_class: nfs-csi
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
