# tsolo.kubernetes/helm/reflector

## Table of content

- [Default Variables](#default-variables)
  - [helm_reflector_chart_version](#helm_reflector_chart_version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### helm_reflector_chart_version

#### Default value

```YAML
helm_reflector_chart_version: 6.1.47
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
