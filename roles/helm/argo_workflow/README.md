# tsolo.kubernetes/helm/argo_workflow

Install Argo Workflow

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
  - [helm_argo_workflow_chart_version](#helm_argo_workflow_chart_version)
  - [helm_argo_workflow_database](#helm_argo_workflow_database)
  - [helm_argo_workflow_hostname](#helm_argo_workflow_hostname)
  - [helm_argo_workflow_sso_token](#helm_argo_workflow_sso_token)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### helm_argo_workflow_chart_version

The version of the Argo Workflow chart that will be installed.

#### Default value

```YAML
helm_argo_workflow_chart_version: 5.1.13
```

### helm_argo_workflow_database

PostgreSQL database credentials used by Argo Workflow.

#### Default value

```YAML
helm_argo_workflow_database:
  address: postgresql.srv
  username: argo
  password: argo
  database: argo
```

### helm_argo_workflow_hostname

Hostname used for Argo Workflow UI.

#### Default value

```YAML
helm_argo_workflow_hostname: argo.{{ internal_domain }}
```

### helm_argo_workflow_sso_token

Keycloak SSO token for Argo client.

#### Default value

```YAML
helm_argo_workflow_sso_token: 1235678901234567890abcdefabcdef
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
