# roles/helm/argo_workflow/tasks/main.yaml
---
- name: Add Bitnami charts repository
  kubernetes.core.helm_repository:
    name: bitnami
    repo_url: https://charts.bitnami.com/bitnami

- name: Prepare internal variabled
  ansible.builtin.set_fact:
    sso_client_id: argo
    sso_client_token: "{{ helm_argo_workflow_sso_token }}"
    argo_url: http://{{ helm_argo_workflow_hostname }}

- name: Create Argo namespace
  kubernetes.core.k8s:
    name: argo
    api_version: v1
    kind: Namespace
    state: present

- name: Argo Workflow SSO secret
  kubernetes.core.k8s:
    state: present
    definition:
      kind: Secret
      metadata:
        name: argo-workflow-sso
        namespace: argo
      data:
        client_id: "{{ sso_client_id | b64encode }}"
        client_secret: "{{ sso_client_token | b64encode }}"

- name: Deploy Argo Workflow
  kubernetes.core.helm:
    name: argo-workflow
    chart_ref: bitnami/argo-workflows
    release_namespace: argo
    chart_version: "{{ helm_argo_workflow_chart_version }}"
    update_repo_cache: true
    values:
      global:
        storageClass: nfs-csi
      ingress:
        enabled: true
        hostname: "{{ helm_argo_workflow_hostname }}"
        ingressClassName: traefik
      server:
        extraArgs:
          - --auth-mode=sso # - "--auth-mode=client"
        auth:
          enabled: true # "{{ helm_keycloak_hostname | default(false) | bool }}"
          mode: sso
          sso:
            enabled: true
            config:
              clientId:
                name: argo-workflow-sso
                key: client_id
              clientSecret:
                name: argo-workflow-sso
                key: client_secret
              redirectUrl: "{{ argo_url }}/oauth2/callback"
              issuer: https://{{ helm_keycloak_hostname | default('keycloak.srv') }}/realms/master
            scope: openid profile
            rbac:
              enabled: false
      controller:
        metrics:
          enabled: false
      postgresql:
        enabled: false
      mysql:
        enabled: false
      externalDatabase:
        enabled: true
        host: "{{ helm_argo_workflow_database.address }}"
        username: "{{ helm_argo_workflow_database.username }}"
        password: "{{ helm_argo_workflow_database.password }}"
        database: "{{ helm_argo_workflow_database.database }}"
        port: 5432
        type: postgresql

- name: Get the Argo Workflow Server Service Account
  kubernetes.core.k8s_info:
    api_version: v1
    kind: ServiceAccount
    name: argo-workflow-argo-workflows-server
    namespace: argo
  register: _service_account

- name: Get the secrets used by Service Account
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Secret
    name: "{{ _service_account.resources[0].secrets[0].name }}"
    namespace: argo
  register: _service_account_secret

- name: Copy the token to the local system
  block:
    - name: Get token from Secret
      ansible.builtin.set_fact:
        _argo_artifact:
          url: "{{ argo_url }}"
          token: Bearer {{ _service_account_secret.resources[0].data.token | b64decode }}

    - name: Create token file
      ansible.builtin.copy:
        content: "{{ _argo_artifact | to_nice_yaml }}"
        dest: /tmp/ansible-skooner-sa-token
        mode: "0744"

    - name: Copy token file
      ansible.builtin.fetch:
        src: /tmp/ansible-skooner-sa-token
        dest: "{{ artifacts_dir }}/argos-workflow-token-{{ helm_argo_workflow_hostname }}.yaml"
        flat: true
        validate_checksum: false
      become: false
