# Getting Started

A short description for using the Tsolo Kubernetes collection for Ansible.

## Prerequisite

Ansible (ansible-core 2.10 or later) and `make` is needed to use the Collection.


## Installation

---

**NOTE**

This Collection is not yet accessable from [Ansible Galaxy](https://galaxy.ansible.com/).

---

To make use of this collection you need to checkout and install the code.

```
git clone https://bitbucket.org/tsolo-dev/ansible-collection-kubernetes.git
cd ansible-collection-kubernetes
make
```

## Example

A minimal working example of using the Tsolo Kubernetes collection.

### Setup an Ansible environment

Start from a clean directory.
```
sudo apt install python3-venv sshpass
mkdir -p  ./host_vars ./group_vars
python -m venv venv
source ./venv/bin/activate
pip install ansible dnspython netaddr jmespath
```

### Inventory
The Ansible inventory for this test will only have a single Node/Host.

For this example the inventory is stored as the file *hosts*.
```
[k8s_initial_controller]
# The first controller to be setup.
test

[k8s_controllers]
# The controller hosts.
test

[k8s_workers]
# The worker hosts.
test

[k0s:children]
k8s_workers
k8s_controllers
k8s_initial_controller
```

The group **k8s_initial_controller** should only have one host defined,
this is the host that will be deployed first.
The initial controller is also the controller that Ansible will connect to when
applications are installed and configured,
e.g. this is where `helm` will be run from.

The group **k8s_controllers** is all the controllers for the Kubernetes cluster,
including the initial controller. It is recomended to have three controller hosts.

The group **k8s_workers** is all the workers for the Kubernetes cluster.

Hosts can be both controller and worker, but it is highly recomended for
production clusters to keep controllers and workers seperate.
In production three controllers are needed.
When the cluster is setup with a high availability (HA) IP address atleast
three worker hosts are needed.

### Host definition

In our inventory we defined on host. Next we need to define the connection
parameters for this host. For this example we call the host *test* and store
the definition in *host_vars/test.yaml*.

```yaml
---

ansible_host: 192.168.1.1
ansible_user: ubuntu
ansible_password: ansible123
ansible_become_password: ansible123
```

### Prepare hosts

It is nesesary that all the hosts have `podman` installed.
It is also recomended to setup recomended NTP and disable swap.

A handly playbook to prepare hosts/nodes for Kubernetes can be found in the
[Tsolo Node](https://bitbucket.org/tsolo-dev/ansible-collection-node) collection.
```shell
ansible-playbook -i hosts tsolo.node.nodes
```

### Install Kubernetes

```shell
ansible-playbook -i hosts tsolo.kubernetes.deploy
```
